﻿
using DuaTaxi.Common.Handlers;
using DuaTaxi.Common.MailKit;
using DuaTaxi.Common.RabbitMq;
using DuaTaxi.Common.Types;
using DuaTaxi.SMTP.Builder;
using DuaTaxi.SMTP.Messages.Commands;
using DuaTaxi.SMTP.Repository;
using DuaTaxi.SMTP.Services.Mailer2;
using MailKit.Net.Smtp;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;

using System.Threading.Tasks;

namespace DuaTaxi.SMTP.Handler.ReserPassword
{
    public class ReserPasswordHandler : ICommandHandler<ResetPasswordVerification>
    {
        private IContactFormRepository _contactFormRepository;
        private IBusPublisher _busPublisher;
        private readonly ILogger<Contact> _logger;
        private readonly MailKitOptions _options;
        private readonly IMailService _mailService;
        private IHostingEnvironment Environment;

        public ReserPasswordHandler(IHostingEnvironment _environment, IContactFormRepository contactFormRepository, IBusPublisher busPublisher, ILogger<Contact> logger, MailKitOptions options, IMailService mailService)
        {
            _contactFormRepository = contactFormRepository;
            _busPublisher = busPublisher;
            _logger = logger;
            _options = options;
            _mailService = mailService;
            Environment = _environment;
        }

        public async Task HandleAsync(ResetPasswordVerification command, ICorrelationContext context)
        {
            var body = RenderDynamicTemplate("EmailRedirect.html");                

            body =  body.Replace("!CALL_BACK!", command.CallBackUrl);


            var message = MessageBuilder
             .Create()
             .From(_options.Email)
             .To( command.Email)
             .WithSubject("DuaTaxi Reset password")
             .WithBody(body)
             .Build();

            using (var client = new SmtpClient()) {
                client.Connect(_options.SmtpHost, _options.Port);

                ////Note: only needed if the SMTP server requires authentication
                client.Authenticate(_options.Username, _options.Password);

                client.Send(message);
                client.Disconnect(true);
            }
        }
        private string RenderDynamicTemplate(string filename)
        {
            string path = Path.Combine(this.Environment.WebRootPath, filename);
            //var viewPath = 
            //HttpContext.Current.Server.MapPath(@"Pages" + filename);

            var template = System.IO.File.ReadAllText(path);

            return template;
        }


    }

}

