﻿using DuaTaxi.Common.Messages;
using DuaTaxi.Common.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuaTaxi.SMTP.Messages.Commands
{
    public class ResetPasswordVerification : IIdentifiable, ICommand
    {
        public string CallBackUrl { get; set; }
        public string Email { get; set; }
        public string Token { get; set; }
        public string Id { get; set; }
    }
}
