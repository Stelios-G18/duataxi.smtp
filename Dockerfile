FROM mcr.microsoft.com/dotnet/core/aspnet:2.2-alpine  AS base
WORKDIR /app
ENV ASPNETCORE_URLS http://*:5011
ENV ASPNETCORE_ENVIRONMENT docker
EXPOSE 5011


FROM mcr.microsoft.com/dotnet/core/sdk:2.2-alpine AS build
WORKDIR /src
COPY ["DuaTaxi.SMTP/DuaTaxi.SMTP.csproj", "DuaTaxi.SMTP/"]
RUN dotnet restore "DuaTaxi.SMTP/DuaTaxi.SMTP.csproj"
COPY . .
WORKDIR "/src/DuaTaxi.SMTP"
RUN dotnet build "DuaTaxi.SMTP.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "DuaTaxi.SMTP.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "DuaTaxi.SMTP.dll"]