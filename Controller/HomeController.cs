﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DuaTaxi.Common.Authentication;
using Microsoft.AspNetCore.Mvc;

namespace DuaTaxi.SMTP.Controller
{
    [Route("")]
    [ServiceFilter(typeof(AuthValidation))]
    public class HomeController : ControllerBase
    {     
        
        public IActionResult Get() => Ok("DuaTaxi SmtpApi");
    }
}